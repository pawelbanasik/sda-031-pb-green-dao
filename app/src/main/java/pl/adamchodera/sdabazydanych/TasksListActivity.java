package pl.adamchodera.sdabazydanych;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.adamchodera.sdabazydanych.data.DaoSession;
import pl.adamchodera.sdabazydanych.data.Task;
import pl.adamchodera.sdabazydanych.data.TaskDao;

public class TasksListActivity extends AppCompatActivity implements TasksListAdapter.TaskCompleteListener {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private TasksListAdapter tasksListAdapter;
    private LoadTasksFromDatabaseAsyncTask loadTasksFromDatabaseAsyncTask;
    private SetTaskAsCompletedAsyncTask setTaskAsCompletedAsyncTask;

    // to dodajemy zeby biblioteka dzialala
    private TaskDao taskDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasks_list);
        ButterKnife.bind(this);

        // te trzy linijki dotycza naszego dap
        MyApplication myApplication = ((MyApplication) getApplication());
        final DaoSession daoSession = myApplication.getDaoSession();
        taskDao = daoSession.getTaskDao();

        initTaskList();
    }

    private void initTaskList() {
        tasksListAdapter = new TasksListAdapter(this);
        recyclerView.setAdapter(tasksListAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadTasksFromDatabaseAsyncTask = new LoadTasksFromDatabaseAsyncTask();
        loadTasksFromDatabaseAsyncTask.execute();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (setTaskAsCompletedAsyncTask != null) {
            setTaskAsCompletedAsyncTask.cancel(true);
        }
        loadTasksFromDatabaseAsyncTask.cancel(true);
    }

    @OnClick(R.id.add_task_button)
    public void goToAddNoteActivity() {
        Intent intent = new Intent(this, TaskDetailsActivity.class);
        startActivity(intent);
    }

    @Override
    public void onTaskCompleted(final Task task) {
        setTaskAsCompletedAsyncTask = new SetTaskAsCompletedAsyncTask(task);
        setTaskAsCompletedAsyncTask.execute();
    }

    private class LoadTasksFromDatabaseAsyncTask extends AsyncTask<Void, Void, List<Task>> {

        @Override
        protected List<Task> doInBackground(Void... params) {



            return new ArrayList<>(); // TODO get tasks from database and return them instead of empty list
        }

        @Override
        protected void onPostExecute(final List<Task> tasks) {
            super.onPostExecute(tasks);

            // TODO set tasks to the adapter

            tasksListAdapter.notifyDataSetChanged();
        }
    }

    private class SetTaskAsCompletedAsyncTask extends AsyncTask<String, Void, List<Task>> {

        private final Task task;

        public SetTaskAsCompletedAsyncTask(Task taskEntity) {
            this.task = taskEntity;
        }

        @Override
        protected List<Task> doInBackground(String... params) {
            // TODO set task as completed in database

            return new ArrayList<>(); // TODO get tasks from database and return them instead of empty list
        }

        @Override
        protected void onPostExecute(final List<Task> tasks) {
            super.onPostExecute(tasks);

            // TODO set tasks to the adapter

            tasksListAdapter.notifyDataSetChanged();
        }
    }
}
