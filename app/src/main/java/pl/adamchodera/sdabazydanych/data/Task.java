package pl.adamchodera.sdabazydanych.data;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public final class Task {

    public static final int DEFAULT_ID_FOR_TASK_NOT_SAVED_IN_DB = -1;

    @Id
    private Long id;

    private String title;

    private String description;

    private boolean completed;



    @Generated(hash = 1049256149)
    public Task(Long id, String title, String description, boolean completed) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.completed = completed;
    }

    @Generated(hash = 733837707)
    public Task() {
    }



//    public Task(long id, String title, String description, boolean completed) {
//        this.id = id;
//        this.title = title;
//        this.description = description;
//        this.completed = completed;
//    }
//
//    public Task() {
//        this(-1, "", "", false);
//    }

    public String getStringId() {
        return String.valueOf(id);
    }



    public boolean isTaskInDatabase() {
        // for task which wasn't inserted into database we set default value as -1
        if (id == DEFAULT_ID_FOR_TASK_NOT_SAVED_IN_DB) {
            return false;
        } else {
            return true;
        }
    }

    public boolean getCompleted() {
        return this.completed;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}
