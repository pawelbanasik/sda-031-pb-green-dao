package pl.adamchodera.sdabazydanych;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import pl.adamchodera.sdabazydanych.data.DaoMaster;
import pl.adamchodera.sdabazydanych.data.DaoSession;

/**
 * Created by RENT on 2017-05-27.
 */

public class MyApplication extends Application {

    private DaoSession daoSession;

    @Override
    public void onCreate() {
        super.onCreate();
        final DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(this, "Tasks.db");
        final SQLiteDatabase db = devOpenHelper.getWritableDatabase();
        final DaoMaster daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();

    }

    public DaoSession getDaoSession() {
        return daoSession;
    }

}